/* Hasg Table */
var hash = (String, max) => {
    var hash = 0;
    for(var i = 0; i < String.length; i++){
        hash += String.charCodeAt(i);
    }
    return hash % max;
};

let HashTable = function(){
    let storage = [];
    const storageLimit = 4;

    this.print = function(){
        console.log(storage);
    }
    this.add = function(key, value){
        var index = hash(key, storageLimit);
        if(storage[index] === undefined){
            storage[index] = [
             [key,value]
            ];
        }else{
            var inserted = false;
            for(var i = 0; i < storage[index].length; i++){
                if(storage[index][i][0] === key){
                    storage[index][i][1] = value;
                    inserted = true;
                }
            }
            if(inserted == false){
                storage[index].push([key, value]);
            }
        }
    };

    this.remove = function(key){
        var index = hash(key, storageLimit);
        if(Storage[index].length === 1 && storage[index][0][0] == key){
            delete storage[index];
        }else{
            for(var i = 0; i < storage[index].length; i++){
                if(storage[index][0][i] === key){
                    delete storage[index][i];
                }
            }
        }
    };

    this.lookup = function(key){
        var index = hash(key, storageLimit);
        if(storage[index] === undefined){
            return undefined;
        }else{
            for(var i = 0; i < storage[index].length; i++){
                if(storage[index][i][0] === key){
                    return storage[index][i][1];
                }
            }
        }
    };

};

console.log(hash('Michael', 256));
console.log(hash('Michael', 40));
let ht = new HashTable();
ht.add('Michael', 'firstName');
ht.add('Nyele', 'middleName');
ht.add('Amadi', 'lastName');
console.log(ht.lookup('Michael'));
ht.print();