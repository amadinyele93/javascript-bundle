/* Queues */
function Queue(){
    var collection = [];
    this.print = function(){
        console.log(collection);
    };
    this.enqueue = function(element){
        collection.push(element);
    };
    this.dequeue = function(){
        collection.shift();
    };
    this.front = function(element){
        return collection[0];
    };
    this.size = function(){
       return collection.length;
    }; 
    this.isEmpty = function(){
        return (collection.length === 0);
    };
}